const cssTemplate = `url( "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 $WIDTH 20'%3E %3Cstyle%3E .normal{ font: italic 10px sans-serif; } %3C/style%3E %3Ctext x='0' y='13' class='normal'%3E $CONTROL_TEXT %3C/text%3E %3C/svg%3E");`

export const DIRECTIONAL_CSS = {
    lri: cssTemplate.replace('$WIDTH', '20').replace('$CONTROL_TEXT', 'LRI'),
    rli: cssTemplate.replace('$WIDTH', '20').replace('$CONTROL_TEXT', 'RLI'),
    fsi: cssTemplate.replace('$WIDTH', '20').replace('$CONTROL_TEXT', 'FSI'),
    lre: cssTemplate.replace('$WIDTH', '20').replace('$CONTROL_TEXT', 'LRE'),
    rle: cssTemplate.replace('$WIDTH', '20').replace('$CONTROL_TEXT', 'RLE'),
    lro: cssTemplate.replace('$WIDTH', '20').replace('$CONTROL_TEXT', 'LRO'),
    rlo: cssTemplate.replace('$WIDTH', '21').replace('$CONTROL_TEXT', 'RLO'),
    pdi: cssTemplate.replace('$WIDTH', '20').replace('$CONTROL_TEXT', 'PDI'),
    pdf: cssTemplate.replace('$WIDTH', '21').replace('$CONTROL_TEXT', 'PDF'),
}

export const DIRECTIONAL_CHARS = {
    /** @type {string} LEFT-TO-RIGHT ISOLATE: sets direction to LTR and isolates the embedded content from the surrounding text */
    lri: '\u{2066}',
    /** @type {string} RIGHT-TO-LEFT ISOLATE: sets direction to RTL and isolates the embedded content from the surrounding text */
    rli: '\u{2067}',
    /** @type {string} FIRST-STRONG ISOLATE: isolates the content and sets the direction according to the first strongly typed directional character */
    fsi: '\u{2068}',
    /** @type {string} LEFT-TO-RIGHT EMBEDDING: sets direction to LTR but allows embedded text to interact with surrounding content, so risk of spillover effects */
    lre: '\u{202a}',
    /** @type {string} RIGHT-TO-LEFT EMBEDDING: sets direction to RTL but allows embedded text to interact with surrounding content, so risk of spillover effects */
    rle: '\u{202b}',
    /** @type {string} LEFT-TO-RIGHT OVERRIDE: overrides the bidirectional algorithm to display characters in memory order, progressing from left to right */
    lro: '\u{202d}',
    /** @type {string} RIGHT-TO-LEFT OVERRIDE: overrides the bidirectional algorithm to display characters in memory order, progressing from right to left */
    rlo: '\u{202e}',
    /** @type {string} POP DIRECTIONAL ISOLATE: end tag used for RLI, LRI or FSI */
    pdi: '\u{2069}',
    /** @type {string} POP DIRECTIONAL FORMATTING: end tag used for RLE or LRE and for RLO or LRO */
    pdf: '\u{202c}',
}

export const websiteElements = {
    cssInputId: 'input-css-text',
    formattedOutputId: 'formatted-output',
    formattedOutputContainer: 'formatted-output',
    formattedOutputDiv: 'formatted-output-div',
    formattingButtonsContainer: 'formatting-mark-container',
    htmlInputId: 'input-html-text',
    showFormattingCheckbox: 'show-formatting-checkbox',
    lriFormattingButton: 'insert-lri',
    rliFormattingButton: 'insert-rli',
    fsiFormattingButton: 'insert-fsi',
    lreFormattingButton: 'insert-lre',
    rleFormattingButton: 'insert-rle',
    lroFormattingButton: 'insert-lro',
    rloFormattingButton: 'insert-rlo',
    pdiFormattingButton: 'insert-pdi',
    pdfFormattingButton: 'insert-pdf',
}