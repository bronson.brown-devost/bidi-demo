import { DIRECTIONAL_CHARS, websiteElements } from '/scripts/constants.js'

const htmlInput = document.getElementById(websiteElements.htmlInputId)
const outputElement = document.getElementById(websiteElements.formattedOutputId)
const showFormattingSwitch = document.getElementById(websiteElements.showFormattingCheckbox)
if (htmlInput === null || outputElement === null || showFormattingSwitch == null) {
    alert('There is a problem with the website, please try reloading')
}

/**
 * Create the buttons for inserting unicode BDI marks
 */
const addBDIFormattingButtons = () => {
    const buttonContainer = document.getElementById(websiteElements.formattingButtonsContainer)
    if (buttonContainer === null) {
        alert('There is a problem with the website, please try reloading')
        return
    }

    const buttons = Object.entries(DIRECTIONAL_CHARS).map(([label, char]) => {
        const formattingButton = document.createElement('button')
        formattingButton.innerText = label
        formattingButton.onclick = () => {
            const updatedText = insertFormattingMark(htmlInput.value, htmlInput.selectionStart, char)
            console.info(updatedText)
            htmlInput.value = updatedText
            showTextInViewer()
        }
        return formattingButton
    })
    buttonContainer.replaceChildren(...buttons)

    
}

/**
 * Scaffold the logic for turning visibility of formatting marks on and off
 */
const addShowFormattingFunctionality = () => {
    showFormattingSwitch.oninput = () => {
        for (const el of document.getElementsByClassName('control')) {
            if (showFormattingSwitch.value)
                el.classList.remove('hidden')
            else
                el.classList.add('hidden')
        }
    }
}

/**
 * 
 * @param {string} text The text in which the BDI control character is to be inserted
 * @param {number} insertLocation The index in the `text`at which the control character should be inserted
 * @param {string} formattingMark The BDI Control Character to insert
 */
const insertFormattingMark = (text, insertLocation, formattingMark) =>
    text.slice(0, insertLocation) + formattingMark + text.slice(insertLocation, text.length)

/**
 * Takes the input `char` and prepends a span to it which will display the BDI control character type.
 * If the input character is not a BDI control character, it will be returned unchanged.
 * 
 * @param {string} char a single character 
 * @returns 
 */
const addBDIFormatting = (text) => {
    const hidden = showFormattingSwitch.value ? '' : ' hidden'
    for (const [label, char] of Object.entries(DIRECTIONAL_CHARS)) {
        text = text.replaceAll(char, `<span class="control ${label}${hidden}"></span>${char}`)
    }
    return text
}

const showTextInViewer = () => {
    const text = htmlInput.value
    const newHTML = document.createElement('div')
    newHTML.id = websiteElements.formattedOutputDiv
    newHTML.innerHTML = addBDIFormatting(text)

    const existingOutputDiv = document.getElementById(websiteElements.formattedOutputDiv)
    if (existingOutputDiv !== null)
        existingOutputDiv.remove()
    
    outputElement.appendChild(newHTML)
}


/* Initialize the site */
htmlInput.oninput = () => {
    showTextInViewer()
}
addShowFormattingFunctionality()
addBDIFormattingButtons()